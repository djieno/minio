#!/bin/bash


sudo systemctl stop minio.service
sudo deluser --remove-home minio-user
sudo delgroup minio-group

sudo systemctl disable minio.service
sudo rm /etc/default/minio /etc/systemd/system/minio.service
sudo rm -fr /mnt/disk1/.minio.sys

