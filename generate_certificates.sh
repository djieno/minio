#!/bin/bash

# Function to generate a Certificate Authority (CA)
generate_ca() {
    openssl genpkey -algorithm RSA -out ca.key
    openssl req -x509 -new -key ca.key -out ca.crt -subj "/C=NL/ST=Noord-Holland/O=Security Dept./L=Amsterdam/OU=none/CN=CA" -days 3650
}

# Function to generate a certificate signing request (CSR)
generate_csr() {
    local domain=$1
    openssl genpkey -algorithm RSA -out "$domain.key"
    openssl req -new -key "$domain.key" -out "$domain.csr" -subj "/C=NL/ST=Noord-Holland/O=Security Dept./L=Amsterdam/OU=none/CN=$domain" -days 3650
}

# Function to sign a certificate using the CA
sign_certificate() {
    local domain=$1
    openssl x509 -req -in "$domain.csr" -CA ca.crt -CAkey ca.key -CAcreateserial -out "$domain.crt" -days 3650
}

# Generate CA
generate_ca

# Generate and sign certificates for each domain
for domain in minio1.lan minio2.lan minio3.lan; do
    generate_csr "$domain"
    sign_certificate "$domain"
done

# Clean up CSR files (optional)
rm *.csr

echo "Certificates generated and signed successfully."
