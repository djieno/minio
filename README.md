# Minio

## Description
This repos contains some installation or help with running minio scripts from a vanilla debian lite rasp server. The starting point is a clean untouched rpi server and install all needed applications to run minio.

## Installation
Use any vanilla raspi debian. Simply copy the line below.
```
sudo curl -sL https://gitlab.com/djieno/minio/-/raw/main/minio-create.sh?ref_type=heads | sudo bash
```

## Deinstallation
```
sudo curl -sL https://gitlab.com/djieno/minio/-/raw/main/cleanup_minio-create.sh?ref_type=heads | sudo bash
```
