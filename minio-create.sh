#!/bin/bash

# validate the current user is root
if [ "$USER" != "root" ]
  then
        echo "This script must be run as user root or sudo $1" 1>&2
        exit 1
fi


ensure_user_and_group_exist() {
    local group_name=$1
    local user_name=$2

    # Check if the group already exists
    if getent group $group_name >/dev/null; then
        echo "Group '$group_name' already exists."
    else
        # Create the group if it doesn't exist
        sudo groupadd $group_name
        echo "Group '$group_name' created."
    fi

    # Check if the user already exists
    if id -u $user_name >/dev/null 2>&1; then
        echo "User '$user_name' already exists."
    else
        # Create the user if it doesn't exist, add to the group
        sudo useradd -m -s /bin/bash -g $group_name $user_name
        echo "User '$user_name' created and added to group '$group_name'."
    fi
}


add_minio.service() {
curl -sL https://gitlab.com/djieno/minio/-/raw/main/minio.service?ref_type=heads -o /etc/systemd/system/minio.service
echo "created minio.service file"
}

add_default_minio() {
curl -sL https://gitlab.com/djieno/minio/-/raw/main/minio_defaults?ref_type=heads -o /etc/default/minio

echo "added default settings for minio"
}

enable_minio_service() {
systemctl enable minio.service
systemctl start minio.service
echo "enabled minio.service"
echo "started minio.service"
}

chown_data_dir() {
chown -R minio-user:minio-group /mnt/disk1
}

create_certs_dir() {
mkdir -p /opt/minio/certs
chown -R minio-user:minio-group /opt/minio/certs/
}

create_certificates() {
curl -sL  https://github.com/minio/certgen/releases/download/v1.2.1/certgen-linux-amd64 -o /opt/minio/certs/certgen-amd64
chown minio-user:minio-group /opt/minio/certs/certgen-amd64
chmod +x /opt/minio/certs/certgen-amd64
/opt/minio/certs/certgen-amd64 -host "minio-amd64-1.lan, minio-amd64-2.lan, minio-amd64-3.lan, storage.lan"
mv public.crt private.key /opt/minio/certs
chown minio-user:minio-group /opt/minio/certs/*.crt /opt/minio/certs/*.key
}

#todo / create function in here that:
# verifiy attached usb disk is present and meet expected disk name etc
# verify mounted disk is ext4 formatted
# verify disk1 is added to fstab
# verify disk1 is properly mounted before enabling / starting minio.service
# tls for minio server


## main

ensure_user_and_group_exist "minio-group" "minio-user"
add_minio.service
add_default_minio
chown_data_dir
create_certs_dir
create_certificates

# enable_minio_service

